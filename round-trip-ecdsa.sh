#!/bin/sh

#export PKCS11_SOFTHSM2_MODULE=/usr/lib/opensc-pkcs11.so

echo "==========> BUILDING"
cargo build

echo "==========> CREATING BACKSIG"
./target/debug/create-backsig --pin 112233 --id 2 < primary.pub.pgp  > backsig.pgp

echo "==========> BINDING SUBKEY"
./target/debug/bind-subkey --backsig backsig.pgp --subkey subkey.pub.pgp < primary.sec.asc > subkey-binding.pgp

echo "==========> INSPECTING"
cat primary.pub.pgp subkey.pub.pgp subkey-binding.pgp  | sq inspect

