use cryptoki::types::object::{Attribute, AttributeType};
use cryptoki::types::session::UserType;
use cryptoki::types::Flags;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "list-objects")]
struct Opt {
    /// PKCS#11 module to load
    #[structopt(short, long)]
    module: String,

    /// Serial number of the token to use
    #[structopt(short, long)]
    serial_number: String,

    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.module, &opt.serial_number, &opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // pub key template
    let pub_key_template = vec![Attribute::Token(true.into())];

    let objects = session.find_objects(&pub_key_template)?;

    for object in &objects {
        eprintln!("Object: {:?}", object);
        let values = session.get_attributes(
            *object,
            &[
                AttributeType::Id,
                AttributeType::Label,
                AttributeType::Value,
                AttributeType::ObjectId,
            ],
        )?;
        for value in &values {
            match value {
                Attribute::Label(str) => eprintln!(" Label: {}", String::from_utf8_lossy(&str)),
                Attribute::Id(id) => eprintln!(" ID: {:?}", id),
                Attribute::Value(bytes) => eprintln!(" Value: {}", base64::encode(bytes)),
                Attribute::ObjectId(bytes) => eprintln!(" ObjectID: {:X?}", bytes),
                _ => {}
            }
        }
        eprintln!();
    }
    Ok(())
}
