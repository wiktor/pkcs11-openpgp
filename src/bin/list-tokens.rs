use cryptoki::types::locking::CInitializeArgs;
use cryptoki::Pkcs11;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "list-tokens")]
struct Opt {
    /// PIN to access the card
    #[structopt(short, long)]
    module: String,
}

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    let pkcs11 = Pkcs11::new(opt.module)?;

    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    eprintln!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    let slots = pkcs11.get_slots_with_token()?;
    for slot in slots {
        println!(
            "{}",
            pkcs11
                .get_token_info(slot)
                .map_or("???".to_string(), |info| { info.get_serial_number() })
        );
    }

    Ok(())
}
