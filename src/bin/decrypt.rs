use cryptoki::types::object::{KeyType, ObjectClass, ObjectHandle};
use cryptoki::types::session::{Session, UserType};
use cryptoki::types::Flags;

use cryptoki::types::mechanism::Mechanism;
use cryptoki::types::object::{Attribute, AttributeType};

use std::{convert::TryInto, io, time::SystemTime};

use sequoia_openpgp as openpgp;

use openpgp::{
    crypto::SessionKey,
    packet::{key::PublicParts, prelude::*},
    parse::{
        stream::{DecryptionHelper, DecryptorBuilder, MessageStructure, VerificationHelper},
        Parse,
    },
    policy::NullPolicy,
    types::SymmetricAlgorithm,
};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "decrypt")]
struct Opt {
    /// PKCS#11 module to load
    #[structopt(short, long)]
    module: String,

    /// Serial number of the token to use
    #[structopt(short, long)]
    serial_number: String,

    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.module, &opt.serial_number, &opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // get mechanism
    let _mechanism = Mechanism::RsaPkcsKeyPairGen;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::Id(vec![opt.id]),
        Attribute::Class(ObjectClass::PUBLIC_KEY),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    //eprintln!("{:?}", objects);

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(
        object,
        &[
            AttributeType::Modulus,
            AttributeType::PublicExponent,
            AttributeType::EcPoint,
        ],
    )?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;
    let mut key_point: Option<Vec<u8>> = None;

    for attribute in attributes {
        match attribute {
            Attribute::Modulus(modulus) => {
                //eprintln!("GOT MODULUS = {:X?}", modulus);
                key_modulus = Some(modulus)
            }
            Attribute::PublicExponent(exponent) => {
                //eprintln!("GOT EXPONENT = {:X?}", exponent);
                key_exponent = Some(exponent);
            }
            Attribute::EcPoint(point) => {
                //eprintln!("GOT POINT = {:X?} ({})", point, point.len());
                // DER decode point
                // https://bugzilla.mozilla.org/show_bug.cgi?id=480280
                let mut p = vec![0x04];
                p.extend(&point[3..]);
                //eprintln!("CNV POINT = {:X?} ({})", p, p.len());
                key_point = Some(p);
            }
            _ => {}
        }
    }

    let key4: Key<PublicParts, key::UnspecifiedRole> =
        if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into()
        } else if let Some(point) = key_point {
            Key4::new(
                SystemTime::UNIX_EPOCH,
                openpgp::types::PublicKeyAlgorithm::ECDH,
                openpgp::crypto::mpi::PublicKey::ECDH {
                    curve: openpgp::types::Curve::NistP256,
                    q: point.into(),
                    hash: openpgp::types::HashAlgorithm::SHA256,
                    sym: openpgp::types::SymmetricAlgorithm::AES256,
                },
            )?
            .into()
        } else {
            panic!("did not get both");
        };

    // priv key template
    let priv_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(true.into()),
        Attribute::Id(vec![opt.id]),
        Attribute::Class(ObjectClass::PRIVATE_KEY),
    ];

    // generate a key pair
    //let (public, private) = session
    //    .generate_key_pair(&mechanism, &pub_key_template, &priv_key_template)?;

    let mut objects = session.find_objects(&priv_key_template)?;

    let handle = objects.remove(opt.index);

    let helper = Helper {
        session,
        handle: &handle,
        key: &key4,
    };

    // Now, create a decryptor with a helper using the given Certs.
    let stdin = std::io::stdin();
    let policy = &NullPolicy::new();
    let mut decryptor = DecryptorBuilder::from_reader(stdin)?.with_policy(policy, None, helper)?;

    // Decrypt the data.
    let mut stdout = std::io::stdout();
    io::copy(&mut decryptor, &mut stdout)?;

    Ok(())
}

struct Helper<'a> {
    session: Session<'a>,
    handle: &'a ObjectHandle,
    key: &'a Key<PublicParts, key::UnspecifiedRole>,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        // Return public keys for signature verification here.
        Ok(Vec::new())
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        // Implement your signature verification policy here.
        Ok(())
    }
}

impl<'a> DecryptionHelper for Helper<'a> {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        let mut pair = PkcsKeyPair {
            session: &self.session,
            handle: self.handle,
            key: self.key,
        };
        //key.into_keypair().unwrap();

        pkesks[0]
            .decrypt(&mut pair, sym_algo)
            .map(|(algo, session_key)| decrypt(algo, &session_key));

        // XXX: In production code, return the Fingerprint of the
        // recipient's Cert here
        Ok(None)
    }
}

struct PkcsKeyPair<'a> {
    session: &'a Session<'a>,
    handle: &'a ObjectHandle,
    key: &'a Key<PublicParts, key::UnspecifiedRole>,
}

impl openpgp::crypto::Decryptor for PkcsKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, key::UnspecifiedRole> {
        self.key
    }

    fn decrypt(
        &mut self,
        ciphertext: &openpgp::crypto::mpi::Ciphertext,
        _plaintext_len: Option<usize>,
    ) -> openpgp::Result<SessionKey> {
        use openpgp::crypto::mpi::Ciphertext::*;
        match ciphertext {
            RSA { c } => {
                let bytes = c.value();
                let decrypted = self
                    .session
                    .decrypt(&Mechanism::RsaPkcs, *self.handle, &bytes)?;
                eprintln!("Decrypted session key = {:X?}", decrypted);
                Ok(decrypted.as_slice().into())
            }
            ECDH { ref e, .. } => {
                eprintln!("e = {:X?}", e);
                let field_sz = 256;

                use cryptoki::types::mechanism::elliptic_curve::*;

                let bytes = e.value();
                eprintln!("Bytes = {:?} {}", bytes, bytes.len());

                let params = Ecdh1DeriveParams {
                    kdf: EcKdfType::NULL,
                    shared_data_len: 0_usize.try_into()?,
                    shared_data: std::ptr::null(),
                    public_data_len: (*bytes).len().try_into()?,
                    public_data: bytes.as_ptr() as *const std::ffi::c_void,
                };

                eprintln!("x");
                let res = self.session.derive_key(
                    &Mechanism::Ecdh1Derive(params),
                    *self.handle,
                    &[
                        Attribute::Class(ObjectClass::SECRET_KEY),
                        Attribute::KeyType(KeyType::GENERIC_SECRET),
                        Attribute::Token(false.into()),
                        Attribute::Sensitive(false.into()),
                        Attribute::Extractable(true.into()),
                        Attribute::Encrypt(true.into()),
                        Attribute::Decrypt(true.into()),
                        Attribute::Wrap(true.into()),
                        Attribute::Unwrap(true.into()),
                    ],
                );
                if let Ok(key) = res {
                    eprintln!("What we got: {:?}", key);
                    let mut value = None;
                    for attribute in self.session.get_attributes(key, &[AttributeType::Value])? {
                        if let Attribute::Value(val) = attribute {
                            value = Some(val);
                        }
                    }

                    eprintln!("What value: {:X?}", value);
                    let value = value.unwrap();
                    eprintln!("Len = {}", value.len());
                    //Err(openpgp::Error::InvalidOperation("not bad".to_string()).into())

                    let mut value = value;
                    while value.len() < (field_sz + 7) / 8 {
                        value.insert(0, 0);
                    }

                    let ret = openpgp::crypto::ecdh::decrypt_unwrap(
                        self.public(),
                        &value.into(),
                        ciphertext,
                    );
                    if let Err(ref e) = ret {
                        println!("Err = {:?}", e);
                    }
                    ret
                } else {
                    eprintln!("Err = {:?}", res);
                    Err(
                        openpgp::Error::InvalidOperation("something bad happened".to_string())
                            .into(),
                    )
                }
            }
            _ => Err(openpgp::Error::InvalidOperation(
                "Don't know how to handle non-RSA things.".to_string(),
            )
            .into()),
        }
    }
}
