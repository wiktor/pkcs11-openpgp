use cryptoki::types::locking::CInitializeArgs;
use cryptoki::types::slot_token::Slot;
use cryptoki::types::Flags;
use cryptoki::Pkcs11;

use cryptoki::types::object::ObjectHandle;
use cryptoki::types::session::{Session, UserType};

use cryptoki::types::mechanism::Mechanism;

use std::sync::{Arc, Mutex};

use openpgp::packet::key::{PublicParts, UnspecifiedRole};
use openpgp::types::HashAlgorithm;
use openpgp::{crypto::mpi::MPI, packet::Key};
use sequoia_openpgp as openpgp;

pub fn init_pins(
    module: &str,
    serial_number: &str,
    pin: &str,
) -> Result<(Pkcs11, Slot), Box<dyn std::error::Error>> {
    let pkcs11 = Pkcs11::new(module)?;

    // initialize the library
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    eprintln!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    // find a slot, get the first one
    let slots = pkcs11.get_slots_with_token()?;

    //    let mut slot = Some(slots.remove(0));
    let mut slot = None;

    for available_slot in slots {
        if serial_number
            == pkcs11
                .get_token_info(available_slot)
                .map_or("???".to_string(), |info| info.get_serial_number())
        {
            slot = Some(available_slot);
        }
    }

    if slot.is_none() {
        panic!("No slot with given serial number");
    }

    let slot = slot.unwrap();

    eprintln!("Slot: {:?}", slot);

    pkcs11.set_pin(slot, &pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(false).set_serial_session(true);

    {
        // open a session
        let session = pkcs11.open_session_no_callback(slot, flags)?;
        // log in the session
        session.login(UserType::User)?;
    }

    eprintln!("Slot OK.");

    Ok((pkcs11, slot))
}

pub enum AlgoMechanism {
    Rsa,
    Ecdsa,
}

pub struct PkcsKeyPair<'a> {
    public: &'a Key<PublicParts, UnspecifiedRole>,
    private_object: &'a ObjectHandle,
    session: Arc<Mutex<Session<'a>>>,
    mechanism: AlgoMechanism,
}

impl<'a> PkcsKeyPair<'a> {
    pub fn new(
        public: &'a Key<PublicParts, UnspecifiedRole>,
        private_object: &'a ObjectHandle,
        session: Arc<Mutex<Session<'a>>>,
        mechanism: AlgoMechanism,
    ) -> Self {
        Self {
            public,
            private_object,
            session,
            mechanism,
        }
    }
}

impl openpgp::crypto::Signer for PkcsKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        self.public
    }

    fn sign(
        &mut self,
        hash_algo: HashAlgorithm,
        digest: &[u8],
    ) -> openpgp::Result<openpgp::crypto::mpi::Signature> {
        //assert_eq!(hash_algo, HashAlgorithm::SHA256);

        eprintln!("Key handle = {:?}", self.private_object);

        use picky_asn1_x509::{AlgorithmIdentifier, DigestInfo, SHAVariant};

        let digest: Vec<_> = digest.into();
        eprintln!(
            "Digest = {:X?} ({}) ({:?})",
            digest,
            digest.len(),
            hash_algo
        );

        let info = if let AlgoMechanism::Rsa = self.mechanism {
            eprintln!("Signing through DigestInfo");
            picky_asn1_der::to_vec(&DigestInfo {
                oid: AlgorithmIdentifier::new_sha(SHAVariant::SHA2_512),
                digest: digest.into(),
            })?
        } else {
            eprintln!("Signing directly");
            digest[0..32].into()
        };

        let session = Arc::clone(&self.session);
        let session = session.lock().unwrap();

        let mechanism = match self.mechanism {
            AlgoMechanism::Rsa => Mechanism::RsaPkcs,
            AlgoMechanism::Ecdsa => Mechanism::Ecdsa,
        };

        let signature = session.sign(&mechanism, *self.private_object, &info)?;

        //self.session.verify(&Mechanism::RsaPkcs, *self.public_object, digest, &signature)?;

        eprintln!(" SIG = {:X?}, len = {}", signature, signature.len());

        if let AlgoMechanism::Rsa = self.mechanism {
            Ok(openpgp::crypto::mpi::Signature::RSA {
                s: signature.into(),
            })
        } else {
            let (r, s) = signature.split_at(signature.len() / 2);
            //let r: Vec<_> = r.into();
            //let s: Vec<_> = s.into();
            eprintln!("r = {:X?} ({}), s = {:X?} ({})", r, r.len(), s, s.len());
            Ok(openpgp::crypto::mpi::Signature::ECDSA {
                r: MPI::new(&r),
                s: MPI::new(&s),
            })
        }
    }
}
